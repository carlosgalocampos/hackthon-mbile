import React, { Component } from "react";
import { TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import BlankPage2 from "../blankPage2";
import DrawBar from "../DrawBar";
import { DrawerNavigator, NavigationActions } from "react-navigation";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right
} from "native-base";
import { Grid, Row } from "react-native-easy-grid";

import { setIndex } from "../../actions/list";
import { openDrawer } from "../../actions/drawer";
import styles from "./styles";

class Home extends Component {
  static navigationOptions = {
    header: null
  };
  static propTypes = {
    name: React.PropTypes.string,
    setIndex: React.PropTypes.func,
    list: React.PropTypes.arrayOf(React.PropTypes.string),
    openDrawer: React.PropTypes.func
  };

  newPage(index) {
    this.props.setIndex(index);
    Actions.blankPage();
  }

  render() {
    console.log(DrawNav, "786785786");
    return (
      <Container style={styles.container}>
        <Header>
          <Left>

            <Button
              transparent
              onPress={() => {
                DrawerNav.dispatch(
                  NavigationActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: "Home" })]
                  })
                );
                DrawerNav.goBack();
              }}
            >
              <Icon active name="power" />
            </Button>
          </Left>

          <Body>
            <Title>Project Mobile</Title>
          </Body>

          <Right>
            <Button
              transparent
              onPress={() => DrawerNav.navigate("DrawerOpen")}
            >
              <Icon active name="menu" />
            </Button>
          </Right>
        </Header>
        <Content>
          <Grid style={styles.mt}>
            {this.props.list.map((item, i) => (
              <Row key={i}>

                <TouchableOpacity
                  style={styles.row}
                  onPress={() =>
                    this.props.navigation.navigate("BlankPage", {
                      name: { item }
                    })}
                >
                    <View style={{flexDirection:'row', flexWrap:'wrap'}}><Text style={styles.text}>{item}</Text></View>
                    <View style={{flexDirection:'row', flexWrap:'wrap'}}>
                      <Text style={{fontSize:11}}>Priority: </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>{i}</Text>
                      <Text style={{fontSize:11}}>  Source:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>Tagalog</Text>
                      <Text style={{fontSize:11}}>  Targets:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>English (US)</Text>
                      <Text style={{fontSize:11}}>  Document Due:  </Text>
                    </View>
                    <View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:10}}>
                      <Text style={{fontSize:11}}>Words:   </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>300</Text>
                      <Text style={{fontSize:11}}>  Tags:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>50</Text>
                      <Text style={{fontSize:11}}>  Status:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>In Progress</Text>
                      <Text style={{fontSize:11}}>  Progress:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>30%</Text>
                    </View>
                </TouchableOpacity>
              </Row>
            ))}
          </Grid>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    openDrawer: () => dispatch(openDrawer())
  };
}
const mapStateToProps = state => ({
  name: state.user.name,
  list: state.list.list
});

const HomeSwagger = connect(mapStateToProps, bindAction)(Home);
const DrawNav = DrawerNavigator(
  {
    Home: { screen: HomeSwagger },
    BlankPage2: { screen: BlankPage2 }
  },
  {
    contentComponent: props => <DrawBar {...props} />
  }
);
const DrawerNav = null;
DrawNav.navigationOptions = ({ navigation }) => {
  DrawerNav = navigation;
  return {
    header: null
  };
};
export default DrawNav;
