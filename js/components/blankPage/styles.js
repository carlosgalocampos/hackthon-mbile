
const React = require('react-native');

const { StyleSheet } = React;
export default {
  container: {
    backgroundColor: '#FBFAFA',
  },
  row: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#F2F3F4',
        margin: 10,
        width: 100,
        height: 150
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: 'flex-start'
  },
  mt: {
    marginTop: 18,
  },
};
