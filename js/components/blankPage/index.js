import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body
} from "native-base";
import { Grid, Row } from "react-native-easy-grid";
import { TouchableOpacity, View } from "react-native";

import styles from "./styles";

class BlankPage extends Component {
  static navigationOptions = {
    header: null
  };
  static propTypes = {
    name: React.PropTypes.string,
    index: React.PropTypes.number,
    list: React.PropTypes.arrayOf(React.PropTypes.string),
    openDrawer: React.PropTypes.func
  };

  render() {
    const { props: { name, index, list } } = this;
    console.log(this.props.navigation, "000000000");
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title>{this.props.navigation.state.params.name.item ? this.props.navigation.state.params.name.item : "lank age"}</Title>
          </Body>

          <Right />
        </Header>

        <Content padder>
          <Grid style={styles.mt}>
              <Row key="1">

                <TouchableOpacity
                  style={styles.row}
                  onPress={() =>
                    this.props.navigation.navigate("DocumentPage", {
                      name: "Translate"
                    })}
                >
                    <View style={{flexDirection:'row', flexWrap:'wrap'}}><Text style={styles.text}>Translate</Text></View>

                    <View style={{flexDirection:'row', flexWrap:'wrap'}}>
                      <Text style={{fontSize:11}}>Task Type: </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>Assignment</Text>
                      <Text style={{fontSize:11}}>  Phase Type:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>Translation</Text>
                    </View>
                    <View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:10}}>
                      <Text style={{fontSize:11}}>Resources:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>Assigned to Mobile Account</Text>
                    </View>
                    <View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:10}}>
                      <Text style={{fontSize:11}}>Start Date:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>28 Nov 2017 4:37 PM</Text>
                      <Text style={{fontSize:11}}>  Completed Date:   </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>300</Text>
                      <Text style={{fontSize:11}}>  Due Date:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>50</Text>
                    </View>
                    <View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:10}}>
                      <Text style={{fontSize:11}}>Status:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>In Progress</Text>
                      <Text style={{fontSize:11}}>  Progress:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>30%</Text>
                    </View>
                </TouchableOpacity>
              </Row>
              <Row key="2">

                <TouchableOpacity
                  style={styles.row}
                  onPress={() =>
                    this.props.navigation.navigate("DocumentPage", {
                      name: "Translate"
                    })}
                >
                    <View style={{flexDirection:'row', flexWrap:'wrap'}}><Text style={styles.text}>Review</Text></View>

                    <View style={{flexDirection:'row', flexWrap:'wrap'}}>
                      <Text style={{fontSize:11}}>Task Type: </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>Assignment</Text>
                      <Text style={{fontSize:11}}>  Phase Type:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>Review</Text>
                    </View>
                    <View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:10}}>
                      <Text style={{fontSize:11}}>Resources:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>Not Assigned</Text>
                    </View>
                    <View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:10}}>
                      <Text style={{fontSize:11}}>Start Date:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>28 Nov 2017 4:37 PM</Text>
                      <Text style={{fontSize:11}}>  Completed Date:   </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>300</Text>
                      <Text style={{fontSize:11}}>  Due Date:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>50</Text>
                    </View>
                    <View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:10}}>
                      <Text style={{fontSize:11}}>Status:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>In Progress</Text>
                      <Text style={{fontSize:11}}>  Progress:  </Text><Text style={{fontSize:11, fontWeight: 'bold'}}>20%</Text>
                    </View>
                </TouchableOpacity>
              </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer())
  };
}

const mapStateToProps = state => ({
  name: state.user.name,
  index: state.list.selectedIndex,
  list: state.list.list
});

export default connect(mapStateToProps, bindAction)(BlankPage);
