import React from "react";
import { AppRegistry, Image, TouchableOpacity } from "react-native";
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content,
  Icon
} from "native-base";
const routes = [
  {id:"Home", desc: "Save Offline"},
  {id:"Home", desc: "Spelling and Grammar Check"},
  {id:"Home", desc: "Term Highlight"},
  {id:"Home", desc: "Create Feedback"},
  {id:"Home", desc: "Auto Format Segment"},
  {id:"Home", desc: "Go To next format tag"}
];
export default class DrawBar2 extends React.Component {
  static navigationOptions = {
    header: null
  };
  render() {
    return (
      <Container>
        <Content>
          <Image
            source={{
              uri: "https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/drawer-cover.png"
            }}
            style={{
              height: 120,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              style={{
                height: 120,
                alignSelf: "stretch",
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => this.props.navigation.navigate("DrawerClose")}
            >
              <Image
                square
                style={{ height: 80, width: 70 }}
                source={{
                  uri: "https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/logo.png"
                }}
              />
            </TouchableOpacity>
          </Image>
          <List
            dataArray={routes}
            renderRow={data => {
              console.log(data)
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data.id)}
                >
                  <Text>{data.desc}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}
