import React, { Component } from "react";
import { connect } from "react-redux";
import DrawBar from "../DrawBar0";
import BlankPage2 from "../blankPage2";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View
} from "native-base";
import { TextInput} from "react-native";
import { DrawerNavigator, NavigationActions } from "react-navigation";

class DocumentPage extends Component {
  static navigationOptions = {
    header: null
  };
  render() {
    const { props: { name, index, list } } = this;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() =>DrawerNav.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title>Workbench</Title>
          </Body>

          <Right>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="ios-menu" />
            </Button>
          </Right>
        </Header>

        <Content padder>
          <View>
            <Text style={{fontWeight: 'bold'}}>
              Original Text
            </Text>
            <Text style={{fontSize:12}}>
            レイアウトを見ると、読者がページの読みやすいコンテンツに注意をそらすのは、長い間確立された事実です。 Lorem Ipsumの使用のポイントは、「ここのコンテンツ、ここのコンテンツ」を使用するのではなく、読みやすい英語のように見える、文字の正規分布が多少あるということです。多くのデスクトップパブリッシングパッケージやWebページエディタでは、デフォルトのモデルテキストとしてLorem Ipsumが使用されています。「lorem ipsum」の検索ではまだ初期段階の多くのWebサイトが公開されます。さまざまなバージョンが何年にもわたって、時には偶然、ときには目的に応じて（注射されたユーモアなど）進化してきました。
            </Text>
          </View>
          <View style={{paddingTop: 10}}>
            <Text style={{fontWeight: 'bold'}}>
                Current Translation
            </Text>
            <TextInput
                    multiline = {true}
                    numberOfLines = {10}
                    style={{borderColor: 'gray', borderWidth: 1, textAlignVertical: 'top'}}
                    defaultValue="It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
            />
          </View>
          <Button title="Learn More" color="#841584">
            <Text>Save and Next</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    openDrawer: () => dispatch(openDrawer())
  };
}
const mapStateToProps = state => ({
  name: state.user.name,
  list: state.list.list
});
const HomeSwagger = connect(mapStateToProps, bindAction)(DocumentPage);
const DrawNav = DrawerNavigator(
  {
    Home: { screen: HomeSwagger },
    BlankPage2: { screen: BlankPage2 }
  },
  {
    contentComponent: props => <DrawBar {...props} />
  }
);
const DrawerNav = null;
DrawNav.navigationOptions = ({ navigation }) => {
  DrawerNav = navigation;
  return {
    header: null
  };
};
export default DrawNav;
