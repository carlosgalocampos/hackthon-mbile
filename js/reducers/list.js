import type { Action } from "../actions/types";
import { SET_INDEX } from "../actions/list";

export type State = {
  list: string
};

const initialState = {
  list: [
    "The Great Gatsby",
    "The Grapes of Wrath",
    "Nineteen Eighty-Four",
    "Ulysses",
    "Lolita",
    "Catch-22"
  ],
  selectedIndex: undefined
};

/*
<Text style={{fontSize:11}}>Priority:  </Text>
<Text style={{fontSize:11}}>  Source:  </Text>
<Text style={{fontSize:11}}>  Targets:  </Text>
<Text style={{fontSize:11}}>  Document Due:  </Text>
</View>
<View style={{flexDirection:'row', flexWrap:'wrap', paddingTop:20}}>
<Text style={{fontSize:11}}>Words:   </Text>
<Text style={{fontSize:11}}>  Tags:  </Text>
<Text style={{fontSize:11}}>  Status:  </Text>
<Text style={{fontSize:11}}>  Progress:  </Text>
*/

export default function(state: State = initialState, action: Action): State {
  if (action.type === SET_INDEX) {
    return {
      ...state,
      selectedIndex: action.payload
    };
  }
  return state;
}
